__precompile__(true)
"""
OMRemote is used to remote control OpenModelica from Julia.
This library is an extension of
[OMJulia](https://github.com/OpenModelica/OMJulia.jl)
providing single functions calls to run a simulation.
"""
module OMRemote

  # (C) Christian Kral, 2017-2022, BSD-3 license
  # OMRemote remote access to run OpenModelica simulations from Julia
  # https://gitlab.com/christiankral/RemoteOM.git


  # V4.0.0 2022-02-19
  #     Switch to lates version of OMJulia based on function ModelicaSystem
  #     Remove obsolete function calls
  #
  # V3.2.0 2022-02-08
  #     Do not overwrite environment variable OPENMODELICAHOME if already set
  #
  # v3.1.0 2021-04-24
  #     Add __precompile__
  #     Add parameter simDir to function simulate
  #     Add parameter verbose; default = false
  #
  # v3.0.0 2021-04-20
  #     Switch to modularized function calls
  #     Remove sysVers parameter and indicate version number through sysLibs
  #         by using the @ spearator, e.g. "Modelica@3.2.3"
  #     Rename
  #         workDir => dir
  #         workFiles => files
  #     Changed default values
  #         copy = true => false
  #         dir = "/work" => "" # No default directory is used
  #     Also return OpenModelica omc handle as first argument
  #
  # v2.10.0
  #
  # v2.9.0

  using OMJulia
  using Base

  export simulateOM
  export loadFile

  # May be removed in a future version
  include("loadFile.jl")

  """
  # Function call

  resFile = `simulateOM(model; sysLibs = "Modelica@3.2.3", file = "", dir = "",
  StopTime = 0, Tolerance = 1E-6, verbose = false)`

  # Description

  This function simulates and OpenModelica model and returns the file name
  of the result file.

  Make sure that the environment variable OPENMODELICAHOME is set properly, e.g.
  by `ENV["OPENMODELICAHOME=/usr/bin"]`. This environment variable is not set
  automatically any more (as in versions 3.X.X).

  # Variables

  `model` Modelica experiment to be simulated (incl. dot notation)

  `sysLibs` Modelica system libraries which are included by the Modelica `uses`
  annotation do not have to be specified here. Additionally  further libraries
  may be specified by the library names separated  by `:`; an additional version
  number is indicated by the '@' separator, e.g. \"Modelica@3.2.3\"

  `file` Modelica file to be loaded; if no path is provided file is loaded
  form directory `dir`; otherwise the absolute file path has to be specified in
  `file` in order to read the file properly

  `dir` Working directory; default value = ""

  `StopTime` Stop time of simulation; if the StopTime <= StartTime, the original
  setting from the Experiment annotation is used; default value = 0

  `Tolerance` Simulation tolerance; default value = 1E-6

  `verbose` Log work progress if true; default = false
  """
  function simulateOM(model; sysLibs = "Modelica", file = "",
                      dir = "", StopTime = 0, Tolerance = 1E-6, verbose = false)

        localDir = pwd()

        # Load system libraries
        sysLibList = split(sysLibs, ":")
        sysLibListLength = size(sysLibList, 1)
        sysLibArray = []
        for sysLib in sysLibList
          lib = split(sysLib, "@")
          if size(lib,1) == 1
            append!(sysLibArray,[lib[1]])
          else
            append!(sysLibArray,[(lib[1],lib[2])])
          end
        end

        # Temporarely switch to work directory if dir is truly a directory
        if isdir(dir)
            cd(dir)
            OMJulia.sendExpression(omc, "cd(\"" * dir * "\")")
        end

        # Create ModelicaSystem object
        mod = OMJulia.OMCSession()
        if sysLibs == ""
          ModelicaSystem(mod,file,model)
        else
          ModelicaSystem(mod,file,model,sysLibArray)
        end

        # Build model
        status = buildModel(mod)
        if verbose
            if status != nothing
              println("    buildModel: ", status)
            end
        end

        # Pass (some) simulation options if not default
        if StopTime != 0
            setSimulationOptions(mod,["stopTime=" * StopTime])
        end
        if Tolerance != 1E-6
            setSimulationOptions(mod,["tolerance=" * Tolerance])
        end

        # Simulate model
        status = OMJulia.simulate(mod)

        # Switch back to original directory before exiting at the next block
        cd(localDir)
        OMJulia.sendExpression(mod, "cd(\"" * localDir * "\")")
        # Copy result file to work directory if copy == true
        return mod
    end
end
